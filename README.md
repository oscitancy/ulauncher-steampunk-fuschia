# ulauncher-steampunk-fuschia

A theme for Ulauncher.

## How to use

1. Create the user-themes directory at ~/.config/ulauncher/user-themes
2. Clone this project
3. Copy the steampunk-fuschia folder into the user-themes directory
4. Select the theme in Ulauncher preferences (You may need to restart Ulauncher)
